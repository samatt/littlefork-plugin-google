# Littlefork Google Module

You need at least Node 7.5.0 for this module.

## Plugins

*Options*:

- `google.headless`: Set to `true` to show the browser window, otherwise browse
  headless if set to `false`. Defaults to `false`.

  `littlefork -Q google_search:Keith\ Johnstone -p google_search,tap_printf --google.headless false`

### `google_search`

Search on Google for a term, specified by the query type `google_search`.

```
littlefork -Q google_search:Keith\ Johnstone -p google_search,tap_printf
```

### `google_images`

Make a Google image search for a term, specified by the query type `google_search`.

```
littlefork -Q google_search:Keith\ Johnstone -p google_images,tap_printf
```

### `google_reverse_images_files`

Make a Google reverse image search. Specify a glob pattern by the query type
`glob_pattern` that resolves to a list of image files.

```
littlefork -Q glob_pattern:images/**/*.jpg -p google_reverse_images_files
```
